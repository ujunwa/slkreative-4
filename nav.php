

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
		<header id="header" class="full-header">

			<div id="header-wrap">

				<div class="container clearfix">

					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

					<!-- Logo
					============================================= -->
					<div id="logo">
						<a href="index.php" class="standard-logo" data-dark-logo="http://assets/images/loo.jpg"><img src="assets/images/loo.jpg" alt="SL Kreativez Logo"></a>
						<a href="index.php" class="retina-logo" data-dark-logo="http://assets/images/loo.jpg"><img src="assets/images/loo.jpg" alt="SL Kreativez Logo"></a>
					</div><!-- #logo end -->

					<!-- Primary Navigation
					============================================= -->
					
					<nav id="primary-menu">

						<ul>
							<li><a href="index.php"><div>Home</div></a>
							</li>
							<li><a href="about.php"><div>Who We Are</div></a>
							</li>
							<li><a href="services.php"><div>What We Do</div></a>
							</li>
							<li><a href="/sl"><div>Blog</div></a>
							</li>
							<li><a href="jobs.php"><div>Careers</div></a>
							</li>
							<li><a href="contact.php"><div>Connect</div></a>
							</li>
							<li><a href="projects.php"><div>Projects</div></a>
							</li>
						</ul>

					</nav><!-- #primary-menu end -->

				</div>

			</div>

		</header><!-- #header end -->